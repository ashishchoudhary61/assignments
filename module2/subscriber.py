import json
from sync_data import sync_mongo
from confluent_kafka import Consumer
from confluent_kafka.cimpl import KafkaException

consumer = None
try:
    conf = {
        'bootstrap.servers': 'localhost:9092, localhost:9093, localhost:9094', 'group.id': "temp", 'session.timeout.ms': 6000,
        'auto.offset.reset': 'earliest'
    }

    consumer = Consumer(conf)
    topics = ["demo-topic"]
    consumer.subscribe(topics)
    while True:
        msg = consumer.poll(timeout=1.0)

        if msg is None:
            continue
        if msg.error():
            raise KafkaException(msg.error())
        else:
            json_message = json.loads(msg.value())
            # print(json_message['id'])
            sync_mongo(json_message['id'])


except Exception as e:
    print(e)

finally:
    if consumer is not None:
        consumer.close()
