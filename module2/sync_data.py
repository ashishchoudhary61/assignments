import psycopg2
import psycopg2.extras
import json
import pprint
from urllib.request import urlopen
from pymongo import MongoClient
from env_config import *


def sync_mongo(id):
    try:

        with db_pg.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            cur.execute('SELECT * from book WHERE id = %s ;', (id, ))
            record = cur.fetchone()
            print(record['name'], record['genre'],
                  record['date_added'], record['isbn'])
            mydict = {"id": record['id'], "name": record['name'], "genre": record['genre'], "ISBN": record['isbn'],
                      "date_time": record['date_added']}

            api_url = "https://www.googleapis.com/books/v1/volumes?q=isbn:"
            response = urlopen(api_url+record['isbn'])
            book_data = json.load(response)
            # pprint.pprint(book_data)
            if book_data["totalItems"] != 0:
                mydict['priview_url'] = book_data["items"][0]['volumeInfo']['previewLink']
                mydict['description'] = book_data["items"][0]['volumeInfo']["description"]
            # print(mydict)
            cur.execute(
                'UPDATE book SET is_added_to_mongo = true WHERE id  = %s;', (id, ))
            db_mongo.book.insert_one(mydict)
            db_pg.commit()
    except Exception as e:
        print(e)
