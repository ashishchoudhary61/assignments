import argparse
import psycopg2.extras
from env_config import *
import sys
import json
from uuid import uuid4
from confluent_kafka import Producer
from datetime import datetime
cur = None


try:
    parser = argparse.ArgumentParser(
        description='Inserting values to postgres database')
    parser.add_argument('name', type=str, help='Name of book')
    parser.add_argument('genre', type=str, help='Genre of book')
    parser.add_argument('ISBN', type=str, help='ISBN number of book')
    args = parser.parse_args()

    cur = db_pg.cursor(cursor_factory=psycopg2.extras.DictCursor)
    create_script = ''' CREATE TABLE IF NOT EXISTS book (
                                    id      SERIAL PRIMARY KEY,
                                    name    varchar(40) NOT NULL,
                                    genre   varchar(40),
                                    date_added timestamp default NULL,
                                    isbn varchar(40) NOT NULL,
                                    is_added_to_mongo BOOLEAN NOT NULL default 'no') '''

    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    cur.execute(create_script)

    db_pg.commit()
    insert_script = 'INSERT INTO book (name, genre, date_added,isbn) VALUES (%s, %s, %s,%s) RETURNING id'
    cur.execute(
        insert_script, (args.name, args.genre, dt_string, args.ISBN))
    jsonv = {"id": cur.fetchall()[0][0]}

    producer = Producer({'bootstrap.servers': 'localhost:9092, localhost:9093, localhost:9094',
                         'socket.timeout.ms': 100})

    def delivery_callback(err, msg):
        if err:
            sys.stderr.write('%% Message failed delivery: %s\n' % err)
        else:
            sys.stderr.write('%% Message delivered to %s [%d] @ %d\n' %
                             (msg.topic(), msg.partition(), msg.offset()))
    producer.poll(0)
    db_pg.commit()
    producer.produce(topic="demo-topic", key=str(uuid4()),
                     value=json.dumps(jsonv), callback=delivery_callback)
    producer.flush()


except Exception as e:
    print(e)
finally:
    if cur is not None:
        cur.close()
