from pymongo import MongoClient
import psycopg2

t_dbname = "Library"
client = MongoClient("mongodb://localhost:27017/")
db_mongo = client[t_dbname]

db_pg = psycopg2.connect(
    "host=localhost port=5432 dbname=Test user=postgres password=123456789")
db_schema = 'public'
db_pg.autocommit = True
