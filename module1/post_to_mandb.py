import argparse
import psycopg2
import psycopg2.extras
from pymongo import MongoClient
from datetime import datetime, timedelta
import random

hostname = 'localhost'
database = 'Test'
username = 'postgres'
pwd = '123456789'
port_id = 5432
conn = None
cur = None

try:
    conn = psycopg2.connect(
        host=hostname,
        dbname=database,
        user=username,
        password=pwd,
        port=port_id
    )

    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    create_script = ''' CREATE TABLE IF NOT EXISTS book (
                                    id      SERIAL PRIMARY KEY,
                                    name    varchar(40) NOT NULL,
                                    genre   varchar(40),
                                    date_added timestamp default NULL,
                                    is_added_to_mongo BOOLEAN NOT NULL default 'no') '''

    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    cur.execute(create_script)

    conn.commit()

    insert_script = 'INSERT INTO book (name, genre, date_added) VALUES (%s, %s, %s)'
    insert_values = [('Angels and Demons', 'Thriller', dt_string),
                     ('Angels and Demons', 'Thriller', dt_string), ('Angels and Demons', 'Thriller', dt_string)]

    for record in insert_values:
        cur.execute(insert_script, record)

    conn.commit()

    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    given_time = datetime.strptime(dt_string, "%d/%m/%Y %H:%M:%S")

    fin_time = given_time - timedelta(minutes=1)
    cur.execute(
        'SELECT * from book WHERE date_added > %s AND is_added_to_mongo = false ;', (fin_time, ))

    client = MongoClient("mongodb://localhost:27017/")
    database = client["Library"]

    for record in cur.fetchall():
        print(record['name'], record['genre'], record['date_added'])
        mydict = {"id": record['id'], "name": record['name'], "genre": record['genre'],
                  "date_time": record['date_added']}
        cur.execute(
            'UPDATE book SET is_added_to_mongo = true WHERE id  = %s;', (record['id'], ))
        database.book.insert_one(mydict)
    conn.commit()


except Exception as e:
    print(e)
finally:
    if conn is not None:
        conn.close()
    if cur is not None:
        cur.close()
